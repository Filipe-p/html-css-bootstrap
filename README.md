# HTML CSS and BootStrap 

This is a flash class on HTML, CSS and Bootstrap.

### Agenda

- HTML
- Basics CSS
- Bootstrap 

Objectives:

- build a simple profile page
- Build some components with css 
- Build layouts using Bootstrap

### HTML

Hyper text markup language - Not a logical programing language, works with markups. 

This is the content and the structre. 

It's super important to understand the structure of what you need before you start adding colors and shapes.

HTML works with tags: 

- html
- head (metadata) vs body (actual content on page)
- h1, h2, h3..
- p
- ul, ol, li
- table, th, tr, td
- a
- img
- div

syntax:

```html
# syntax
<tag options="values"> content </tag>
```

## starting html

Start with html:

```html 
<!DOCTYPE html>
<html >

</html>

```

add the head and body

```html
<!DOCTYPE html>
<html>
    <head>
        <!-- metadata -->
        <!-- link to css -->
        <!-- information for browser -->
    </head>
    <body>
        <!-- write content -->
    </body>
</html>
```

## CSS 

CSS stands for cascading style sheets. They add colour, style, spacing and with html make 80% of your webise.

No amount of CSS can make up for non existing or bad HTML structure. 

You can apply CSS in line on the option of a tag or you can import ant target tags. 

Example of in line:

```html
<tag style="font-weight: bold;"> content </tag>
```

Useful from small sections.

Let's link a style sheet in the heather:

```html
<!DOCTYPE html>
<html>
    <head>
        <!-- metadata -->
        <!-- link to css -->
        <link rel="stylesheet" href="style.css">
        <!-- information for browser -->
    </head>
    <body>
        <!-- write content -->
    </body>
</html>
```

### Targeting HTML with CSS 



